import Vue from 'vue';
import Vuex from 'vuex';

// modules
import products from './modules/products';
import collections from './modules/collections';
import cart from './modules/cart';
import toast from './modules/toast';
import scroll from './modules/scroll';
import video from './modules/video';
import kitBuilder from './modules/kitBuilder';
import header from './modules/header';
import offers from './modules/offers';
import fullOverlay from './modules/fullOverlay';
import social from './modules/social';
import swell from './modules/swell';

Vue.use(Vuex);

// map metafield values to class names
const metafieldColorMap = {
  'Primary': 'brand-primary',
  'Secondary': 'brand-secondary',
  'Tertiary': 'brand-tertiary',
  'Accent 1': 'accent-1',
  'Accent 2': 'accent-2',
  'Accent 3': 'accent-3',
  'Accent 4': 'accent-4',
  'Accent 5': 'accent-5',
  'White': 'white',
  'Off-White': 'off-white',
  'Light Grey': 'light-grey',
  'Grey': 'grey',
  'Off-Black': 'off-black',
  'Black': 'black',
}

const state = {
  badgeColorMap: {
    'out of stock': ['bg-off-white', 'color-black'],
    'sale': window.bvaccel.schema.productTile.badges.sale,
    'limited': window.bvaccel.schema.productTile.badges.limited,
    'new': window.bvaccel.schema.productTile.badges.new,
    'best seller': window.bvaccel.schema.productTile.badges.bestSeller,
    'clearance':  window.bvaccel.schema.productTile.badges.clearance,
    'deal of the week': window.bvaccel.schema.productTile.badges.dealOfTheWeek,
  },
  comparePriceColor: window.bvaccel.comparePriceColor,
  config: {},
  metafieldColorMap
};


export default new Vuex.Store({
  state,
  modules: {
    products,
    collections,
    cart,
    toast,
    scroll,
    video,
    kitBuilder,
    header,
    offers,
    fullOverlay,
    social,
    swell,
  },
});
