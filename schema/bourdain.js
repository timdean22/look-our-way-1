const json = JSON.stringify;
const colorOptions = [
  { label: 'Primary', value: 'color-brand-primary' },
  { label: 'Secondary', value: 'color-brand-secondary' },
  { label: 'Tertiary', value: 'color-brand-tertiary' },
  { label: 'Sale', value: 'color-util-sale' },
  { label: 'Accent 1', value: 'color-accent-1' },
  { label: 'Accent 2', value: 'color-accent-2' },
  { label: 'Accent 3', value: 'color-accent-3' },
  { label: 'Accent 4', value: 'color-accent-4' },
  { label: 'Accent 5', value: 'color-accent-5' },
  { label: 'White', value: 'color-white' },
  { label: 'Off White', value: 'color-off-white' },
  { label: 'Light Grey', value: 'color-light-grey' },
  { label: 'Grey', value: 'color-grey' },
  { label: 'Off Black', value: 'color-off-black' },
  { label: 'Black', value: 'color-black' },
];

module.exports = {
  uikit: {
    colorOptions: json(colorOptions),
  },
};
