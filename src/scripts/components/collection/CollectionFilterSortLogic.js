import without from 'lodash/without';
import ShopifyCollectionLogicOR, { getUrlEncodedSort, getUrlEncodedFilters } from './ShopifyCollectionLogicOR';
import { TAG_DELIMITER, SORT_OPTIONS, AVAILABILITY_FILTER_FNS, AVAILABILITY_FILTER_NAMES} from './collection-constants';
import CollectionFiltersView from './CollectionFiltersView.vue';

const DEFAULT_AVAILABILITY_FILTER = AVAILABILITY_FILTER_NAMES[0];

export default {
  props: {
    collection: Object,
    /** Defines what filter categories are shown to the user */
    filterCategories: Array
  },
  components: {
    CollectionFiltersView
  },
  data() {
    const activeFilters = getUrlEncodedFilters() || [];
    const activeSort = getUrlEncodedSort() || SORT_OPTIONS[0].value;
    const tagDelimiter = TAG_DELIMITER;
    const activeAvailabilityFilter = DEFAULT_AVAILABILITY_FILTER;

    return {
      collectionController: new ShopifyCollectionLogicOR(this.collection)
        .filterBy(activeFilters)
        .setCustomFilters([AVAILABILITY_FILTER_FNS[activeAvailabilityFilter]])
        .sortBy(activeSort),
      activeFilters,
      activeSort,
      activeAvailabilityFilter,
      sortOptions: SORT_OPTIONS,
      tagDelimiter,
    }
  },
  methods: {
    onFilterToggle(tag) {
      let newActiveFilters;
      if(this.activeFilters.includes(tag)) {
        newActiveFilters = without(this.activeFilters, tag);
      } else {
        newActiveFilters = [...this.activeFilters, tag];
      }
      this.activeFilters = newActiveFilters;
      this.collectionController.filterBy(newActiveFilters);
    },
    onAvailabilityFilterToggle(value) {
      const defaultFilterFn = k => k;

      this.collectionController
        .setCustomFilters([AVAILABILITY_FILTER_FNS[value] || defaultFilterFn]);
      this.activeAvailabilityFilter = value;
    },
    clearAllFilters() {
      this.activeAvailabilityFilter = DEFAULT_AVAILABILITY_FILTER;
      this.activeFilters = [];
      this.collectionController.clearFilters();
    },
    onSortSelect(value) {
      if(value !== this.activeSort) {
        this.activeSort = value;
        this.collectionController.sortBy(value); 
      }
    }
  },
  computed: {
    filtersList() {
      return this.filterCategories
        .map(category => this.collectionController.allFilters.find((filter) => filter.category.toLowerCase() === category.toLowerCase()))
        .filter(Boolean);
    },
    // format active filters for use as pill buttons
    formattedActiveFilters() {
      return this.activeFilters.map(tag => {
        const [, value] = tag.split(this.tagDelimiter);
        return {
          value,
          remove: function() {
            this.activeFilters = without(this.activeFilters, tag);
            this.collectionController.filterBy(this.activeFilters);
          }.bind(this),
        }
      })
    }
  }
}
