import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState({
      swellCustomer: (state) => {
        return state.swell.customer.length > 0 ? state.swell.customer[0] : false;
      },
      vipTiers: (state) => {
        return state.swell.all.length > 0 ? state.swell.all[0].vipTiers : [];
      },
      redemptionOptions: (state) => {
        return state.swell.all.length > 0 ? state.swell.all[0].redemptionOptions : [];
      },
      swellCampaigns: (state) => {
        return state.swell.all.length > 0 ? state.swell.all[0].activeCampaigns : [];
      },
    }),
  },
  mounted() {
    this.$store.dispatch('swell/init');
  },
};
