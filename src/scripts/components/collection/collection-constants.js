export const AVAILABILITY_FILTER_FNS = {
  'All'                 : product => product,
  'In Stock'            : product => product.available,
  'Sale'                : product => product.price < product.compare_at_price,
  'Out of Stock'        : product => !product.available,
  'Limited Availability': product => product.tags.map(t => t.toLowerCase()).includes('limited stock')
}

export const AVAILABILITY_FILTER_NAMES = ['All', 'In Stock', 'Sale', 'Out of Stock', 'Limited Availability'];

export const SORT_OPTIONS = [
  {label: 'Best selling'       , value: 'best-selling'     },
  {label: 'Alphabetically, A-Z', value: 'title-ascending'  },
  {label: 'Alphabetically, Z-A', value: 'title-descending' },
  {label: 'Price, low to high' , value: 'price-ascending'  },
  {label: 'Price, high to low' , value: 'price-descending' },
  {label: 'Date, old to new'   , value: 'created-ascending'},
  {label: 'Date, new to old'   , value: 'created-decending'},
]


export const TAG_DELIMITER = '::';