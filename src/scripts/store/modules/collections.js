import axios from 'axios'
import { isHiddenProduct } from 'scripts/helpers/util';
import { handleize } from '../../filters/string.js'

const state = {
  fetchingStatus: false,
  currentHandle: "",
  currentPage: 1,
  currentSort: "",
  currentFilters: [],
  allFilterOptions: [],
  currentProducts: [],
  currentTags: [],
  currentTotal: 0,
}

const mutations = {
  SET_FETCHING_STATUS: (state, status) => (state.fetchingStatus = status),
  SET_CURRENT_HANDLE: (state, handle) => (state.currentHandle = handle),
  SET_CURRENT_PAGE: (state, page) => (state.currentPage = page),
  SET_CURRENT_SORT: (state, sort) => (state.currentSort = sort),
  SET_CURRENT_FILTERS: (state, filters) => (state.currentFilters = filters),
  SET_CURRENT_PRODUCTS: (state, products) => (state.currentProducts = products),
  SET_CURRENT_TAGS: (state, tags) => (state.currentTags = tags),
  SET_CURRENT_TOTAL: (state, total) => (state.currentTotal = total),
  SET_CURRENT_FILTER: (state, filter) => {
    state.currentFilters = [filter];
  },
  SAVE_ALL_FILTER_OPTIONS: (state, filters) => {
    state.allFilterOptions = filters;
  },
}

const actions = {
  saveAllFilters({commit}, filters) {
    commit('SAVE_ALL_FILTER_OPTIONS', filters);
  },
  setFilter({commit}, filter) {
    commit('SET_CURRENT_FILTER', filter);
  },
  updateHandle({commit, dispatch}, handle){
    commit('SET_CURRENT_HANDLE', handle)
    dispatch('fetchProducts')
    saveLastCollection()
  },
  updatePage({commit, dispatch}, page) {
    commit('SET_CURRENT_PAGE', page)
    dispatch('fetchProducts')
    saveLastCollection()
  },
  updateSort({commit, dispatch}, sort) {
    commit('SET_CURRENT_SORT', sort)
    commit('SET_CURRENT_PAGE', 1)
    dispatch('fetchProducts')
    saveLastCollection()
  },
  updateFilters({commit, dispatch}, filters) {
    commit('SET_CURRENT_FILTERS', filters)
    commit('SET_CURRENT_SORT', state.currentSort)
    commit('SET_CURRENT_PAGE', 1)
    dispatch('fetchProducts')
    saveLastCollection()
  },
  setInitialCollectionState({commit, dispatch}, init) {
    commit('SET_CURRENT_HANDLE', init.handle)
    commit('SET_CURRENT_PRODUCTS', init.products.filter(prod => !isHiddenProduct(prod)))
    commit('SET_CURRENT_TAGS', init.tags)
    commit('SET_CURRENT_TOTAL', init.total)
    commit('SET_CURRENT_SORT', init.sort)
    if(init.filters) {
      commit('SET_CURRENT_FILTERS', init.filters)
    }
    if(init.page) {
      commit('SET_CURRENT_PAGE', init.page)
    }
    if(init.returning) {
      dispatch('fetchProducts')
    }
    saveLastCollection()
  },
  fetchProducts({commit}){
    commit('SET_FETCHING_STATUS', true)
    let url = filterSortUrlBuilder()
    axios.get(url).then((response) => {
      commit('SET_CURRENT_PRODUCTS', response.data.products.filter(prod => !isHiddenProduct(prod)))
      commit('SET_CURRENT_TAGS', response.data.tags)
      commit('SET_CURRENT_TOTAL', response.data.productCount)
    }).then(() => {
      commit('SET_FETCHING_STATUS', false)
    }).then(() => {
      updateReviews()
    })
  }
}

const updateReviews = () => {
  if ( window && window.SPR ) {
    window.SPR.initDomEls()
    window.SPR.loadProducts()
    window.SPR.loadBadges()
  } else {
    console.log('ERROR: SPR not installed or window unavailable')
  }
}

const filterSortUrlBuilder = () => {
  let url = ""
  let baseUrl = `/collections/${state.currentHandle}/`
  if(state.currentFilters.length){
    let handleizedArray = state.currentFilters.map(filter => handleize(filter))
    let concatenatedTags = handleizedArray.join('+')
    url = `${baseUrl}${concatenatedTags}?view=json&page=${state.currentPage}&sort_by=${state.currentSort}`
  }
  if (!state.currentFilters.length && state.currentSort.length) {
    url = `${baseUrl}?view=json&page=${state.currentPage}&sort_by=${state.currentSort}`
  }
  return url
}

const saveLastCollection = () => {
  const lastCollection = {
    currentHandle: state.currentHandle,
    currentPage: state.currentPage,
    currentSort: state.currentSort,
    currentFilters: state.currentFilters,
    currentProducts: state.currentProducts,
    currentTotal: state.currentTotal,
    currentTags: state.currentTags,
    returning: true
  }
  sessionStorage.setItem('lastCollection', JSON.stringify(lastCollection))
}


export default { namespaced: true, state, mutations, actions }
