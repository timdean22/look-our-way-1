import axios from 'axios'
import partition from 'lodash/partition';
import prop from 'lodash/property';
import isEqual from 'lodash/isEqual';

const CART_MODAL_CLOSE_DELAY_MS = 5000;
// Limit number of products under a given price
const LIMITED_PRODUCT_PRICE = 500;
const LIMITED_PRODUCT_CAP = 3;

const state = {
  checkout: {},
  sidecartOpen: false,
  kits: [],
  cartModalEnabled: window.bvaccel.cart.modalEnabled,
  cartModalOpen: false,
  cartModalTimerId: 0
}

const mutations = {
  SET_CHECKOUT (state, checkout) {
    state.checkout = checkout
    state.checkout.item_count = checkout.items.reduce((count, item) => {
      return item.handle !== 'customizations' ? count + item.quantity : count;
    }, 0);
  },
  OPEN_SIDECART (state) {
    state.sidecartOpen = true
    document.querySelector('body').classList.add('no-scroll');
  },
  OPEN_CARTMODAL (state) {
    state.cartModalOpen = true
  },
  CLOSE_CARTMODAL (state) {
    state.cartModalOpen = false
  },
  CLOSE_SIDECART (state) {
    state.sidecartOpen = false
    document.querySelector('body').classList.remove('no-scroll');
  },
  SET_KITS(state, kits) {
    state.kits = kits
  },
  SET_CART_MODAL_TIMER_ID (state, id) {
    state.cartModalTimerId = id;
  }
}

const actions = {
  init ({ commit, dispatch }) {
    // forcing clean cart request for IE11
    let timestamp = new Date().getTime()
    let kits = []

    kits = findKits()

    axios.get(`/cart.js?q=${timestamp}`)
      .then(response => {
        commit('SET_CHECKOUT', response.data)

        let toRemove = [];

        let customizationPricingItem = response.data.items.filter(item => item.handle === 'customizations');
        customizationPricingItem.forEach(pricingItem => {
          let customizedItem = response.data.items.find(item => {
            return pricingItem.properties._customizedItemId === item.id
              && isEqual(pricingItem.properties._customProperties, item.properties._custom);
          })

          if (!customizedItem) {
            toRemove.push(pricingItem);
          }
        });

        dispatch('removeItems', toRemove);
      })
      .then(() => {
        let kits = findKits()
        dispatch('setKits', kits)
      })
  },
  addItems ({ state, commit, dispatch }, items) {
    if(!(items instanceof Array))
      items = [items];

    // separate items to update from items to add
    const [ itemsToUpdate, itemsToAdd ] = partition(items, item => {
      const isKit = item.properties && !!item.properties._kitId;
      const hasCustomOptions = item.properties && !!item.properties._custom;
      const isCustomPricingProduct = item.properties && !!item.properties._customizedItemId;

      const existingItem = state.checkout.items.find(lineItem => {
        if (isCustomPricingProduct) {
          return lineItem.id === item.id
            && isEqual(lineItem.properties._customProperties, item.properties._customProperties)
            && lineItem.properties._customizedItemId === item.properties._customizedItemId;
        }

        if (hasCustomOptions) {
          return lineItem.id === item.id
            && isEqual(lineItem.properties._custom, item.properties._custom);
        }

        return lineItem.id === item.id;
      });

      return existingItem && !isKit;
    });
    let promise;

    if(itemsToUpdate && itemsToUpdate.length > 0) {
      const lineItemIds = state.checkout.items.map(prop('id'));
      for(const item of itemsToUpdate) {
        // get the matching line item
        const lineItem = state.checkout.items[lineItemIds.indexOf(item.id)];
        // sum quantity
        item.quantity = item.quantity + lineItem.quantity;
        if (item.price < LIMITED_PRODUCT_PRICE && lineItem.handle !== 'customizations') {
          item.quantity = Math.min(LIMITED_PRODUCT_CAP, item.quantity);
        }
        // add line item key
        item.key = lineItem.key;
      }
      promise = dispatch('updateItems', itemsToUpdate);
    }

    if(itemsToAdd && itemsToAdd.length > 0) {
      itemsToAdd.forEach((item) => {
        let lineItem = state.checkout.items.find(li => li.key === item.key);
        if (item.price < LIMITED_PRODUCT_PRICE && lineItem.handle !== 'customizations') {
          item.quantity = Math.min(LIMITED_PRODUCT_CAP, item.quantity);
        }
      });
      const data = {items: itemsToAdd};
      if(promise instanceof Promise) {
        promise.then(() => {
          axios.post('/cart/add.js', data)
        })
      } else{
        promise = axios.post('/cart/add.js', data);
      }
    }

    return promise.then(() => fetch('/cart/update.js', {
        method: 'POST',
        body: JSON.stringify({}),
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
      }))
      .then(() => axios.get(`/cart.js?q=${new Date().getTime()}`))
      .then(response => {
        commit('SET_CHECKOUT', response.data);
        commit('SET_KITS', findKits() || []);
        return response.data;
      })
      .catch(err => {
        console.error(err);
        if (Sentry) { Sentry.captureException(err); }
        dispatch('toast/send', { text: err.message, type: 'error' }, {root: true});
        return err;
      });
  },
  /**
   * @desc NOTE: Only updates quantity.
   *
   * @typedef Item
   * @type {Object}
   * @prop {String} key
   * @prop {Number} quantity
   *
   * @param {Array<Item>} items
   */
  updateItems ({ state, commit, dispatch }, items) {
    if(!(items instanceof Array))
      items = [items];

    const updatedItemKeys = items.map(prop('key'));
    const lineItemQuantities = state.checkout.items
      .map((item) => {
        return items[updatedItemKeys.indexOf(item.key)] || item
      })
      .map((item) => {
        const stateItem = state.checkout.items.find(checkoutItem => checkoutItem.key === item.key);

        if (stateItem && stateItem.price < LIMITED_PRODUCT_PRICE && item.key === stateItem.key && stateItem.handle !== 'customizations') {
          if (item.quantity > LIMITED_PRODUCT_CAP) {
            const toastMessage = `Cannot add more than ${LIMITED_PRODUCT_CAP} of "${stateItem.title}"`;
            dispatch('toast/send', { text: toastMessage, type: 'error' }, {root: true});
          }
          return Math.min(item.quantity, LIMITED_PRODUCT_CAP);
        }

        return item.quantity;
      });

    const payload = {
      updates: lineItemQuantities,
    };

    return axios.post('/cart/update.js', payload)
      .then(response => {
        commit('SET_CHECKOUT', response.data)
        commit('SET_KITS', findKits() || []);
        return response;
      })
      .catch(err => {
        console.error(err);
        if (Sentry) { Sentry.captureException(err); }
        dispatch('toast/send', { text: err.message, type: 'error' }, {root: true});
        return err;
      })
  },
  removeItems ({ dispatch, state }, items) {
    let customProducts = [];
    if(!(items instanceof Array))
      items = [items];


    items.forEach(item => {
      let itemData = state.checkout.items.find(lineItem => lineItem.key === item.key)
      item.quantity = 0;

      if (itemData.properties._custom && itemData.properties._custom.length > 0) {
        customProducts.push(itemData)
      }
    });

    customProducts.forEach(customProduct => {
      let itemToRemove = state.checkout.items.find(item => {
        return item.properties._customizedItemId === customProduct.id
          && isEqual(item.properties._customProperties, customProduct.properties._custom);
      });

      if (itemToRemove) {
        itemToRemove.quantity = 0;
        items.push(itemToRemove);
      }
    })

    return dispatch('updateItems', items);
  },
  openSidecart: ({ commit }) => {
    commit('CLOSE_CARTMODAL');
    commit('OPEN_SIDECART');
  },
  closeSidecart: ({ commit }) => commit('CLOSE_SIDECART'),
  openCartModal: ({ state, commit }) => {
    window.clearTimeout(state.cartModalTimerId);
    commit('OPEN_CARTMODAL');
  },
  closeCartModal: ({ commit }) => commit('CLOSE_CARTMODAL'),
  setKits: ({commit}, kits) => commit('SET_KITS', kits),
  afterAddToCart: ({ state: { cartModalEnabled }, dispatch, commit }) => {
    const action = cartModalEnabled ? 'openCartModal' : 'openSidecart';
    dispatch(action);

    if(action === 'openCartModal') {
      const timerId = setTimeout(() => dispatch('closeCartModal'), CART_MODAL_CLOSE_DELAY_MS);
      commit('SET_CART_MODAL_TIMER_ID', timerId);
    }
  }
}

// Searches current cart line items for those with a _kitId property, adds unique kit ids to
// an array, then separates all kit items into objects matching their unique id and returns
// them for state
const findKits = () => {
  if(state.checkout.items && state.checkout.items.length) {
    let kitItems = state.checkout.items
      .filter(prop('properties'))
      .filter(prop('properties._kitId'))
    let _kitIds = []

    kitItems.forEach(item => {
      if (!_kitIds.includes(item.properties._kitId)) {
        _kitIds.push(item.properties._kitId)
      }
    })

    return _kitIds.map(id => {
      return {
        id: id,
        items: kitItems.filter(item => item.properties._kitId === id)
          .sort((a, b) => b.price - a.price) // sort highest to lowest so lunchbox magnet isn't first
      }
    })
  }
}

const getters = {
  cartModalOpen ({ cartModalEnabled, cartModalOpen, checkout }) {
    return cartModalEnabled && cartModalOpen && checkout.items.length > 0;
  }
}

export default { namespaced: true, state, mutations, actions, getters }
